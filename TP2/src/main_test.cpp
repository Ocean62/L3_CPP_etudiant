#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv) {
    // fuite memoire correction
    MemoryLeakWarningPlugin::turnOffNewDeleteOverloads(); // TODO
    return CommandLineTestRunner::RunAllTests(argc, argv);
}

