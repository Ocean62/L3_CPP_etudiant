#include "Liste.hpp"

Liste::Liste():_tete(nullptr){}

void Liste::ajouterDevant(int valeur) {
    _tete = new Noeud{valeur, _tete};
    /*
    Noeud* n = new Noeud;
    n->_valeur = valeur;
    n->_suivant = _tete;
    _tete = n;
    */
}

int Liste::getTaille() const {
    int nb = 0;
    Noeud * n = _tete;
    while (n) {
        nb++;
        n = n->_suivant;
    }
    return nb;
}

/* A faire
int getElement(int indice) const {

}
*/
Liste::~Liste() {
    while(_tete) {
        Noeud * aSupprimer = _tete;
        _tete = _tete-> _suivant;
        delete aSupprimer;
    }
}

