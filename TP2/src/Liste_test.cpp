#include "Liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, test_liste_1) { // premier test
	Liste l;
    CHECK(l._tete==nullptr);
}

TEST(GroupListe, test_ajouter_1) {
    Liste l;
    l.ajouterDevant(37);
    CHECK(l._tete != nullptr);
    CHECK(l._tete->_valeur == 37);
    CHECK(l._tete->_suivant == nullptr);
}

TEST(GroupListe, test_gettaille_1) {
    Liste l;
    CHECK(l.getTaille() == 0);
}

TEST(GroupListe, test_gettaille_2) {
    Liste l;
    l.ajouterDevant(11);
    l.ajouterDevant(12);
    CHECK(l.getTaille() == 2);
}

/*
TEST(GroupListe, test_getelement_1) {

}
*/
