#include "Doubler.hpp"

#include <iostream>

int main() {
	// Pointeurs
	// entier a
	int a;
	// a a pour valeur 42
	a = 42;
	// pointeur d'entier
	int* p;
	// pointer p vers a
	p = &a;
	// affecter la valeur 37 dans a via le pointeur
	*p = 37;
	std::cout << p << std::endl;
	std::cout << a << std::endl;
	
	// Allocation dynamique
	// pointeur d'entier
	int* t;
	// Allouer un tableau de 10 entiers pointé par t
	t = new int[10];
	// affecter la valeur 42 à la 3eme case
	t[2] = 42;
	// désallouer le tableau
	delete []t;
	// réinitialiser le pointeur à nullptr
	t= nullptr;
	
    //std::cout << doubler(21) << std::endl;
    //std::cout << doubler(21.f) << std::endl;
    return 0;
}

