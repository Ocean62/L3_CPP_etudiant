#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP

#include "couleur.hpp"

class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur & couleur);
    virtual ~FigureGeometrique() = default;
    const Couleur & getCouleur() const;
    virtual void afficher() const = 0;
};

#endif // FIGUREGEOMETRIQUE_HPP
