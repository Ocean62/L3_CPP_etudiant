#ifndef LIGNE_HPP
#define LIGNE_HPP

#include "figuregeometrique.hpp"
#include "point.hpp"

class Ligne : public FigureGeometrique {
private:
    Point _p0;
    Point _p1;
public:
    Ligne(const Couleur & couleur, const Point & P0,
          const Point & P1);
    void afficher() const;
    const Point & getP0() const;
    const Point & getP1() const;
};

#endif // LIGNE_HPP
