#include "figuregeometrique.hpp"
#include "ligne.hpp"

#include <iostream>

int main() {
    Couleur c= {10,0,0};
    Point p1 = {100,0};
    Point p2 = {200,300};

    Ligne l(c,p1,p2);
    l.afficher();
    //std::cout << doubler(21) << std::endl;
    //std::cout << doubler(21.f) << std::endl;

    /**
      #include <vector>
      std::vector<> figures {
        new Ligne({1,0,0}, {0,0}, {100,200}),
        new PolygoneRegulier({0,1,0}, {100,200}, 50, 5)
      };

      for (FigureGeometrique * f : figures)
        f->afficher();

      for (FigureGeometrique * f : figures)
        delete f;
    **/

    return 0;
}
