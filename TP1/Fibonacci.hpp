/*
 * Fibonacci.hpp
 * 
 * Copyright 2020 lesecq ocean <olesecq@straussl>
 * 
 * g++ main.cpp -o tp1
 * ./tp1
 * 
 * git add .
 * git commit -m "hello word"
 */
#ifndef FIBONACCI_HPP_
#define FIBONACCI_HPP_

int fibonacciRecursif(int nb);
int fibonacciIteratif(int nb);

#endif
