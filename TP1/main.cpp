/*
 * main.cpp
 * 
 * Copyright 2020 lesecq ocean <olesecq@straussl>
 * 
 * g++ main.cpp -o tp1
 * ./tp1
 * 
 * git add .
 * git commit -m "hello word"
 * git push
 * 
 * g++ -c main.cpp
 * 
 * g++ [-o fibo] fibo.o main.o
 * 
 * -lm
 * libm.a
 * libn.a
 */

#include <iostream>
#include "Fibonacci.hpp"

#include "Vecteur3.cpp"

using namespace std;

int main() {
	cout << fibonacciRecursif(7) << endl;
	cout << fibonacciIteratif(7) << endl;

    //Vector3D;

    return 0;
}
