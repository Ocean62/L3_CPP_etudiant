// Fibonacci_test.cpp
#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFiba) { };

TEST(GroupFiba, test_1) {
    int result = fibonacciRecursif(1);
    CHECK_EQUAL(1, result);
}

TEST(GroupFiba, test_2) {
    int result = fibonacciRecursif(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFiba, test_3) {
    int result = fibonacciRecursif(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFiba, test_4) {
    int result = fibonacciRecursif(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFiba, test_5) {
    int result = fibonacciRecursif(5);
    CHECK_EQUAL(5, result);
}

TEST(GroupFiba, test_6) {
    int result = fibonacciIteratif(1);
    CHECK_EQUAL(1, result);
}

TEST(GroupFiba, test_7) {
    int result = fibonacciIteratif(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFiba, test_8) {
    int result = fibonacciIteratif(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFiba, test_9) {
    int result = fibonacciIteratif(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFiba, test_10) {
    int result = fibonacciIteratif(5);
    CHECK_EQUAL(5, result);
}
